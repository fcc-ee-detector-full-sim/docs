# Manipulating edm4hep objects

Useful ressources:

- [Tutorial](https://github.com/key4hep/key4hep-tutorials/blob/main/edm4hep_analysis/edm4hep_api_intro.md): see especially the [Reading EDM4hep files](https://github.com/key4hep/key4hep-tutorials/blob/main/edm4hep_analysis/edm4hep_api_intro.md#reading-edm4hep-files) section which explains how to manipulate edm4hep objects
- [Edm4hep object definitions](https://github.com/key4hep/EDM4hep/blob/main/edm4hep.yaml)
- [Edm4hep Doxygen](https://edm4hep.web.cern.ch/index.html): the above `yaml` file is processed by PODIO to create a bunch of C++ classes defining a consistent data model based on the require content. It also creates a set of members (getters and setters). Using this Doxygen page will allow you to see what member is available for which object. 

This data model comes with a lot of relations and associations between objects. For instance you can go from an `edm4hep::ReconstructedParticle` to the associated tracks with the `getTracks()` method, then to the tracker hit used to produced this track with the `getTrackerHits()` method of the returned `edm4hep::Track` object.