# Introduction


This page collects the dimensions for the alternative option of the IDEA detector including an additional dual readout crystal calorimeter to improve EM resolution and gain in longitudional granularity. The only differences of other IDEA subdetectors compared to the nominal geometry are:

- the inner radius of the solenoid is increased from 2100 mm to 2300 mm
- the pre-shower is removed
- the inner radius of the dual-readout fiber calorimeter is increased from 2500 mm to 2700 mm


Please note that the outer radius of the fibre calorimeter is unchanged (the crystal EM section represents the first interaction length of the calorimeter) and thus the muon detector and return yoke geometry is unchanged. Similarly, the vertex detector, the drift chamber and silicon wrapper are unchanged, please refer to the IDEA nominal geometry page as reference for the geometry of these subdetectors.


# Detector Dimensions


![IDEA_dimensions](../img/baseline_radial_evelope_withFiberReduction_noDCchanges.png)
<div style="text-align: center">Schematic layout of the IDEA detector with EM crystal calorimeter section. Disclaimer: the dimensions on this picture serve as indicative reference, real/exact numbers to be used are reported in the following tables.</div>


## Change log
Here is a change log listing recent updates on the baseline geometry which have to be ported to the software implementation:

- ...

The dimensions described below are the dimensions as they are/should be implemented in Full Simulation.


## Dual-readout crystal Calorimeter


### Barrel
| R [mm] | L [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |
| 2100 | ±(2100÷2400) | 300 | 1|




### Endcap
| R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |--- |
| ±(... to ..)  | ±(2100 to 2400)  | ±(2100 to 2400) | 300 | 1 |




## Magnet


|  | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | X0[%] |
|--- |--- |--- |--- |--- |
|Solenoid | 2300 | 2600 | ±2600 | 0.75 |




## End-plate absorber
[to be checked! not changed compared to IDEA baseline]


|  | R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | X0[%] |
|--- |--- |--- |--- |--- |
|Lead | 380 | 2090 | ±(2490 to 2495) | 0.75 |






## Dual-readout fiber Calorimeter


### Barrel
| R [mm] | L [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |
| 2700 | ±(2700÷4500) | 1800 | 8|




### Endcap
| R<sup>in</sup> [mm] | R<sup>out</sup> [mm] | z [mm] | Thickness [mm]| Int. length |
|--- |--- |--- |--- |--- |
| ±(390 to 680)  | ±(2700 to 4500)  | ±(2700 to 4500) | 1800 | 8 |





# Detector builders


Here is a list of detector builders and their contact person:


| Sub-detector             | Detector builder | Software Contact | Hardware Contact |
|---                       |---               |---             |--- |
| Dual readout crystals    | soon             |  Wonyong Chung  | Marco Lucchini |
|---                 |---               |---             | |
| Drift chamber      | [Link](https://github.com/HEP-FCC/FCCDetectors/blob/main/Detector/DetFCCeeIDEA/src/DriftChamber_o1_v00.cpp) | Brieuc Francois | |
| Silicon wrapper    |   To be written | Armin Ilg | |
| Dual-readout calorimeter | [Link](https://github.com/HEP-FCC/dual-readout/blob/master/Detector/DRcalo/src/DRgeo.cpp) | tbd | |
