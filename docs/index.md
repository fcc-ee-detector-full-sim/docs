# Introduction

Welcome to the FCC-ee Full Sim website! This page hosts relevant information regarding FCC-ee detectors implementation, simulation and reconstruction in the Key4hep framework. The goal we are pursuing is to implement all the FCC-ee subdetector geometries and reconstruction in a consistent software framework ([Key4hep](https://key4hep.github.io/key4hep-doc/)) to, among other benefits, be able to easily exchange part of the simulation accross detector concepts. The existing FCC-ee detector implementations are available in the [k4geo](https://github.com/key4hep/k4geo/tree/master/FCCee) GitHub repository.


This website includes recipes to run the various detector simulations, detector dimensions, name of contact persons, pointers to the relevant documentation and cheatsheets for Key4hep/DD4hep (including ddsim) usage or development.

Recent progress, plans, faced difficulties, etc, are discussed at the bi-weekly [FCC Detector Full Sim working meeting](https://indico.cern.ch/category/16938/).

# Contacts

- Brieuc Francois
- Gerardo Ganis