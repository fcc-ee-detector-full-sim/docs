# Introduction

The CLD detector concept has been adapted from CLICdet to fit the FCC-ee evrionment and relies mainly on Silicon based technologies. The full simulation and reconstruction is available for this detector.

![CLD](img/CLD.png)
<div style="text-align: center">Isometric view of the CLD detector, with one quarter removed.</div>


# Recipe (ddsim + MarlinWrappers)

## Simulation

The CLD simulation is based on `ddsim`. Here is an example of to run it with the nightly Key4hep release:

```
source /cvmfs/sw-nightlies.hsf.org/key4hep/setup.sh
ddsim --steeringFile $CLDCONFIG/share/CLDConfig/cld_steer.py \
      --compactFile $K4GEO/FCCee/CLD/compact/CLD_o2_v07/CLD_o2_v07.xml \
      --enableGun \
      --gun.distribution uniform \
      --gun.energy "10*GeV" \
      --gun.particle mu- \
      --numberOfEvents 100 \
      --outputFile Step1_edm4hep.root
```

This will produce an output file in `edm4hep` dataformat with Geant4 SimHits that can then be fed to the reconstruction step. Note that ddsim can also be used with any modern Monte Carlo generator output file with the command line option `--inputFiles` (supported formats are `.stdhep`, `.slcio`, `.HEPEvt`, `.hepevt`, `.hepmc` and `.pairs`). More information:

 - `ddsim -h`
 - [FCC software tutorial](https://hep-fcc.github.io/fcc-tutorials/master/full-detector-simulations/Geometry/Geometry.html)

## Reconstruction

The CLD reconstruction in inherited from the CLIC and ILC reconstruction chains and requires usage of the k4MarlinWrapper for dataformat translation. Here is how to run it:

```
git clone https://github.com/key4hep/CLDConfig.git
cd CLDConfig/CLDConfig
# Modify the detector xml in `CLDReconstruction.py` (geoservice.detectors) to point to the one you used at the SIM step
k4run CLDReconstruction.py --inputFiles <your-file(s)-here> \
                           --outputBasename <basename-of-output-files> \
                           -n <number-of-events-or--1-for-all>
```
The `--inputFiles` argument takes either `.slcio` or `edm4hep.root` files and configures the input translation automatically. The `--outputBasename` argument takes a string that the output files will start with. By default the reconstruction output will be in `<outputBasename>.edm4hep.root` and some diagnostic plots and ntuples will end up in `<outputBasename>.aida.root`.

## Description of the output rootfile content

Here is some information about the content of the Root file produced by the reconstruction step:

- The particle flow object collection, of type `edm4hep::ReconstructedParticle`, is available under the `PandoraPFOs` collection. Looking at the file content with e.g. `podio-dump test_cld_edm4hep.root` will reveal that there are four such collections, one without any selection and three with different selection criteria: `LooseSelectedPandoraPFOs`, `SelectedPandoraPFOs` and `TightSelectedPandoraPFOs`. NB: since those last three collections result from a filtering of `PandoraPFOs`, only references to `PandoraPFOs` objects are stored in the `*SelectedPandoraPFOs`.
- The jets, also of type `edm4hep::ReconstructedParticle`, are stored under `[Refined]VertexJets`. More information can be found in [this paper](https://arxiv.org/pdf/1506.08371.pdf).
- From any `edm4hep::ReconstructedParticle`, you can acces the associated tracks, vertices, ReconstructedParticle (e.g. for jets constituents), etc. Cf the edm4hep documentation.

For more detailed information about the definition of these object, browse `CLDReconstruction.py` and the source code it calls. 

To learn how to manipulate these objects, check the [Manipulating edm4hep objects](../EDM4HEP/index.md) page of this website. 

<!--| Collection name | Content |
|---|---|
| _SiTracks_Refitted_trackStates | TrackState at 4 different points: the IP, the first hit, the last hit and the calorimeter. To know which TrackState correspond to e.g. the one at the IP, use something like myTrackState.location == edm4hep::TrackState::AtIP;  (see [here](https://github.com/key4hep/EDM4hep/blob/main/edm4hep.yaml#L84C26-L84C30)).   |
-->
